#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define FLUSH stdin=freopen(NULL,"r",stdin)

typedef struct bankAccount {
    int accountNumber;
    char *accountName;
    double accountBalance;
    struct bankAccount *nextAccountPtr;
} Account, *AccountPtr;

void printUserInstructions();
void printAllAccounts(AccountPtr currentPtr);
void printAccountInFrontOfQueue(AccountPtr headPtr);
void printAccounAtBackOfQueue(AccountPtr tailPtr);

void addAccount(AccountPtr *headPtr, AccountPtr *tailPtr);
void removeAccount(AccountPtr *headPtr, AccountPtr *tailPtr);

int createAccountNumber(AccountPtr headPtr);

int isNum(char tempInput[]);
int isEmpty(AccountPtr headPtr);

int main(void) {
    AccountPtr headPtr = NULL, tailPtr = NULL;
    int choice;

    do {
        printUserInstructions();
        choice = 0;
        scanf("%d", &choice);
        FLUSH;
        switch (choice) {
            case 1:
                addAccount(&headPtr, &tailPtr);
                break;
            case 2:
                removeAccount(&headPtr, &tailPtr);
                break;
            case 3:
                printAllAccounts(headPtr);
                break;
            case 4:
                printAccountInFrontOfQueue(headPtr);
                break;
            case 5:
                printAccounAtBackOfQueue(tailPtr);
                break;
            case 6:
                printf("Exiting program.\n\n");
                break;
            default:
                printf("Your choice is invalid.\n");
                break;
        }
    } while (choice != 6);

    return 0;
}

void printUserInstructions() {
    puts("What do you want to do? Enter...");
    puts("---> ( 1 ) To add an account");
    puts("---> ( 2 ) To remove an account");
    puts("---> ( 3 ) To print all accounts");
    puts("---> ( 4 ) To print the account in front of the queue"); // back of queue
    puts("---> ( 5 ) To print the account at the back of the queue"); // front of queue
    puts("---> ( 6 ) To exit the program");
    printf("Enter your choice: ");
}

void printAllAccounts(AccountPtr currentPtr) {
    if (currentPtr == NULL) {
        printf("=========================================================================\n");
        printf("No accounts have been added to this bank.\n");
        printf("=========================================================================\n");
    } else {
        printf("=========================================================================\n");
        printf("The accounts in this bank are: \n\n");
        printf("%s %15s %23s \n", "Account Number", "Account Name", "Account Balance");
        printf("%s %15s %23s \n", "--------------", "------------", "---------------");
        while (currentPtr != NULL) {
            printf("%-17d ", currentPtr->accountNumber);
            printf("%-20s ", currentPtr->accountName);
            printf("$%15.2f\n", currentPtr->accountBalance);
            currentPtr = currentPtr->nextAccountPtr;
        }
        printf("=========================================================================\n");
    }
}

void printAccountInFrontOfQueue(AccountPtr headPtr) {
    if (headPtr == NULL) {
        printf("=========================================================================\n");
        printf("No accounts have been added to this bank.\n");
        printf("=========================================================================\n");
    } else {
        printf("The account in front of the queue: \n\n");
        printf("%s %15s %23s \n", "Account Number", "Account Name", "Account Balance");
        printf("%s %15s %23s \n", "--------------", "------------", "---------------");
        printf("%-17d ", headPtr->accountNumber);
        printf("%-20s ", headPtr->accountName);
        printf("$%15.2f\n", headPtr->accountBalance);
        headPtr = headPtr->nextAccountPtr;
    }
}

void printAccounAtBackOfQueue(AccountPtr tailPtr) {
    if (tailPtr == NULL) {
        printf("=========================================================================\n");
        printf("No accounts have been added to this bank.\n");
        printf("=========================================================================\n");
    } else {
        printf("The account at the back of the queue: \n\n");
        printf("%s %15s %23s \n", "Account Number", "Account Name", "Account Balance");
        printf("%s %15s %23s \n", "--------------", "------------", "---------------");
        printf("%-17d ", tailPtr->accountNumber);
        printf("%-20s ", tailPtr->accountName);
        printf("$%15.2f\n", tailPtr->accountBalance);
        tailPtr = tailPtr->nextAccountPtr;
    }
}

void addAccount(AccountPtr *headPtr, AccountPtr * tailPtr) {
    char tempInput[20];
    char accountName[30];
    double accountBalance;
    int confirm;

    do {
        FLUSH;
        printf("Enter account name: ");
        fgets(accountName, sizeof (accountName), stdin);
        accountName[strcspn(accountName, "\n")] = '\0';
        if (strlen(accountName) < 1) {
            printf("\nWARNING: Account name cannot be blank.\n\n");
        }
    } while (strlen(accountName) < 1);

    do {
        FLUSH;
        printf("Enter account balance: ");
        memset(tempInput, '\0', sizeof (tempInput));
        fgets(tempInput, sizeof (tempInput), stdin);
        accountBalance = atof(tempInput);

        accountBalance = (isNum(tempInput) == 0) ? -1 : accountBalance;
        accountBalance = (accountBalance < 0) ? -1 : accountBalance;

        if (accountBalance == -1) {
            printf("\nWARNING: Account balance must be a valid number and cannot be less than 0.\n\n");
        }
    } while (accountBalance < 0);

    do {
        FLUSH;
        printf("Do you want to add this account to the bank? (Y/N): ");
        memset(tempInput, '\0', sizeof (tempInput));
        fgets(tempInput, sizeof (tempInput), stdin);

        if (tempInput[0] == 'Y' || tempInput[0] == 'y') {
            confirm = 1;
        } else if (tempInput[0] == 'N' || tempInput[0] == 'n') {
            confirm = 0;
        } else {
            confirm = -1;
        }

        if (confirm == 1) {
            AccountPtr newPtr = (AccountPtr) malloc(sizeof (Account));
            if (newPtr != NULL) {
                newPtr->accountNumber = createAccountNumber(*headPtr);
                newPtr->accountName = (char*) calloc(strlen(accountName) + 1, sizeof (char));
                if (newPtr->accountName == NULL) {
                    printf("\nERROR: Unable to add account. No memory available.\n\n");
                    exit(1);
                }
                strncpy(newPtr->accountName, accountName, strlen(accountName) + 1);
                newPtr->accountBalance = accountBalance;
                newPtr->nextAccountPtr = NULL;

                if (isEmpty(*headPtr)) {
                    *headPtr = newPtr;
                } else {
                    (*tailPtr)->nextAccountPtr = newPtr;
                }
                *tailPtr = newPtr;
            } else {
                printf("\nERROR: Unable to add account. No memory available.\n\n");
                exit(1);
            }
        } else if (confirm == 0) {
            printf("\nThis account has not been added to the bank.\n\n");
        } else if (confirm == -1) {
            printf("\nWARNING: You must enter Y or N to confirm your choice.\n\n");
        }
    } while (confirm == -1);
}

void removeAccount(AccountPtr *headPtr, AccountPtr * tailPtr) {
    AccountPtr tempPtr;
    char tempInput[10];
    int tempAccountNumber;
    int confirm;

    if (isEmpty(*headPtr)) {
        *tailPtr = NULL;
        printf("=========================================================================\n");
        printf("No accounts have been added to this bank.\n");
        printf("=========================================================================\n");
    } else {
        FLUSH;
        tempAccountNumber = (*headPtr)->accountNumber;
        printf("Do you want to remove account number %d from the bank? (Y/N): ", tempAccountNumber);
        memset(tempInput, '\0', sizeof (tempInput));
        fgets(tempInput, sizeof (tempInput), stdin);

        if (tempInput[0] == 'Y' || tempInput[0] == 'y') {
            confirm = 1;
        } else if (tempInput[0] == 'N' || tempInput[0] == 'n') {
            confirm = 0;
        } else {
            confirm = -1;
        }

        if (confirm == 1) {
            tempPtr = *headPtr;
            *headPtr = (*headPtr)->nextAccountPtr;
            free(tempPtr);
        } else if (confirm == 0) {
            printf("\nAccount number %d has not been removed from the the bank.\n\n", tempAccountNumber);
        } else if (confirm == -1) {
            printf("\nWARNING: You must enter Y or N to confirm your choice.\n\n");
        }
    }
}

int createAccountNumber(AccountPtr headPtr) {
    register int i = 0;
    AccountPtr currentPtr = headPtr;

    while (currentPtr != NULL) {
        if (currentPtr->accountNumber == i) {
            i++;
            currentPtr = headPtr;
        } else {
            currentPtr = currentPtr->nextAccountPtr;
        }
    }
    return i;
}

int isNum(char tempInput[]) {
    int check = 0;
    for (int i = 0; i < strlen(tempInput); i++) {
        if (isdigit(tempInput[i])) {
            ++check;
        }
    }
    return check == (strlen(tempInput) - 1);
}

int isEmpty(AccountPtr headPtr) {
    return headPtr == NULL;
} 