/*
 *  C Project Part A: Accounts in a Bank
 * 	Group Members:
 * 	Arel Jann(AJ) Clemente
 * 	Khalil Shazam
 * 	Jeremiah Torralba
 * 	Axl Patawaran
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#define FLUSH stdin=freopen(NULL,"r",stdin)

typedef struct bankAccount {
    int accountNumber;
    char *accountName;
    double accountBalance;
    struct bankAccount *nextAccountPtr;
} Account, *AccountPtr;

void printUserInstructions();                                   // prints the user instructions
void printTopAccount(AccountPtr topPtr);                        // prints the most recent added account of the bank (stack)
void printAllAccounts(AccountPtr topPtr);                       // prints all the added accounts of the bank (stack)
int is_Num(char string[]);                                      //checks if number
AccountPtr addAccountUserInput(AccountPtr newPtr);              // retrieve and validate user inputs; if all inputs are validated, addAccount() is called to create account 

AccountPtr addAccount(AccountPtr topPtr, char accountName[],
        double accountBalance);                                 // (similar to a push() function) adds a new account to the bank (stack)
AccountPtr removeAccount(AccountPtr topPtr);                    // (similar to a pop() function) removes the most recent added account from the bank (stack)
int createAccountNumber(AccountPtr topPtr);                     // creates an account number for a new accounts

int main() {
    AccountPtr accountPtr = NULL;
    int choice;

    do {
        printUserInstructions();
        choice = 0;
        scanf("%d", &choice);
        FLUSH;
        switch (choice) {
            case 1:
                accountPtr = addAccountUserInput(accountPtr);
                break;
            case 2:
                accountPtr = removeAccount(accountPtr);
                break;
            case 3:
                printAllAccounts(accountPtr);
                break;
            case 4:
                printTopAccount(accountPtr);
                break;
            case 5:
                printf("Exiting program.\n\n");
                break;
            default:
                printf("Your choice is invalid.\n");
                break;
        }
    } while (choice != 5);
    return 0;
}

void printUserInstructions() {
    puts("What do you want to do? Enter...");
    puts("---> ( 1 ) To add an account");
    puts("---> ( 2 ) To remove an account");
    puts("---> ( 3 ) To print all accounts");
    puts("---> ( 4 ) To print the most recent added account");
    puts("---> ( 5 ) To exit the program");
    printf("Enter your choice: ");
}

void printTopAccount(AccountPtr topPtr) {
    if (topPtr == NULL) {
        printf("=========================================================================\n");
        printf("No accounts have been added to this bank.\n");
        printf("=========================================================================\n");
    } else {
        printf("=========================================================================\n");
        printf("The most recent added account is:\n");

        printf("%s %15s %23s \n", "Account Number", "Account Name", "Account Balance");
        printf("%s %15s %23s \n", "--------------", "------------", "---------------");


        printf("%-17d ", topPtr->accountNumber);
        printf("%-20s ", topPtr->accountName);
        printf("%15.2f\n", topPtr->accountBalance);
        printf("=========================================================================\n");
    }
}

void printAllAccounts(AccountPtr currentPtr) {
    if (currentPtr == NULL) {
        printf("=========================================================================\n");
        printf("No accounts have been added to this bank.\n");
        printf("=========================================================================\n");
    } else {
        printf("=========================================================================\n");
        printf("The accounts in this bank are: \n\n");
        printf("%s %15s %23s \n", "Account Number", "Account Name", "Account Balance");
        printf("%s %15s %23s \n", "--------------", "------------", "---------------");
        while (currentPtr != NULL) {
            printf("%-17d ", currentPtr->accountNumber);
            printf("%-20s ", currentPtr->accountName);
            printf("$%15.2f\n", currentPtr->accountBalance);
            currentPtr = currentPtr->nextAccountPtr;
        }
        printf("=========================================================================\n");
    }
}

AccountPtr addAccountUserInput(AccountPtr newPtr) {
    char tempString[20];
    char accountName[30];
    double accountBalance;

    printf("=========================================================================\n");
    do {
        FLUSH;
        printf("Enter account name: ");
        fgets(accountName, sizeof (accountName), stdin);
        accountName[strcspn(accountName, "\n")] = '\0';
        if (strlen(accountName) < 1) {
            printf("\nWARNING: Account name cannot be blank.\n\n");
        }
    } while (strlen(accountName) < 1);

    do {
        FLUSH;
        printf("Enter account balance: ");
        memset(tempString, '\0', sizeof (tempString));
        fgets(tempString, sizeof (tempString), stdin);
        accountBalance = atof(tempString);

        accountBalance = (is_Num(tempString) == 0) ? -1 : accountBalance;
        accountBalance = (accountBalance <= 0) ? -1 : accountBalance;

        if (accountBalance == -1) {
            printf("\nWARNING: Account balance must be a valid number and cannot be less than 0.\n\n");
        }

    } while (accountBalance < 0);
    printf("=========================================================================\n");
    return addAccount(newPtr, accountName, accountBalance);
}

AccountPtr addAccount(AccountPtr topPtr, char accountName[], double accountBalance) {
    char tempString[20];
    int confirm;

    do {
        FLUSH;
        printf("Do you want to add this account? (Y/N):");
        memset(tempString, '\0', sizeof (tempString));
        fgets(tempString, sizeof (tempString), stdin);

        if (tempString[0] == 'Y' || tempString[0] == 'y') {
            confirm = 1;
        } else if (tempString[0] == 'N' || tempString[0] == 'n') {
            confirm = 0;
        } else {
            confirm = -1;
        }

        if (confirm == 1) {
            AccountPtr newPtr = (AccountPtr) malloc(sizeof (Account));
            if (newPtr != NULL) {
                newPtr->accountNumber = createAccountNumber(topPtr);
                newPtr->accountName = (char*) calloc(strlen(accountName) + 1, sizeof (char));
                if (newPtr->accountName == NULL) {
                    printf("\nERROR: Unable to add account. No memory available.\n\n");
                    exit(1);
                }
                strncpy(newPtr->accountName, accountName, strlen(accountName) + 1);
                newPtr->accountBalance = accountBalance;
                newPtr->nextAccountPtr = topPtr;
                topPtr = newPtr;
            } else {
                printf("\nERROR: Unable to add account. No memory available.\n\n");
                exit(1);
            }
            printf("\nThis account has been added to the bank.\n\n");
            return topPtr;
        } else if (confirm == 0) {
            printf("\nThis account has not been added to the bank.\n\n");
        } else if (confirm == -1) {
            printf("\nWARNING: You must enter Y or N to confirm your choice.\n\n");
        }
    } while (confirm == -1);
    return topPtr;
}

AccountPtr removeAccount(AccountPtr topPtr) {
    AccountPtr tempPtr;
    char tempAccountName[30];
    char tempString[20];
    int confirm;

    if (topPtr != NULL) {
        do {
            FLUSH;
            printf("Do you want to remove the most recent added account? (Y/N):");
            memset(tempString, '\0', sizeof (tempString));
            fgets(tempString, sizeof (tempString), stdin);

            if (tempString[0] == 'Y' || tempString[0] == 'y') {
                confirm = 1;
            } else if (tempString[0] == 'N' || tempString[0] == 'n') {
                confirm = 0;
            } else {
                confirm = -1;
            }

            if (confirm == 1) {
                tempPtr = topPtr;
                strncpy(tempAccountName, topPtr->accountName, strlen(topPtr->accountName));
                topPtr = topPtr->nextAccountPtr;
                free(tempPtr);
                printf("\nThe account has been removed from the bank.\n\n");
            } else if (confirm == 0) {
                printf("\nThe account has not been removed from the the bank.\n\n");
            } else if (confirm == -1) {
                printf("\nWARNING: You must enter Y or N to confirm your choice.\n\n");
            }
        } while (confirm == -1);
    } else {
        printf("=========================================================================\n");
        printf("No accounts have been added to this bank.\n");
        printf("=========================================================================\n");
    }
    return topPtr;
}

int createAccountNumber(AccountPtr topPtr) {
    register int i = 0;
    AccountPtr currentPtr = topPtr;

    while (currentPtr != NULL) {
        if (currentPtr->accountNumber == i) {
            i++;
            currentPtr = topPtr;
        } else {
            currentPtr = currentPtr->nextAccountPtr;
        }
    }
    return i;
}

int is_Num(char string[]) {
    int check = 0;
    for (int i = 0; i < strlen(string); i++) {
        if (isdigit(string[i])) {
            ++check;
        }
    }
    return check == (strlen(string) - 1);
}